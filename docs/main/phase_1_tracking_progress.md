# Phase 1: Tracking progress

After completing the activities of a given meeting, please set the meeting as done in the [spreadsheet](https://ethercalc.org/lkcamp-round2).

It is not mandatory, but we are going to use this information to keep track about the overall progress of the class and adapt if we move slower or faster then expected, this is not a competition.  

We plan to have a page and a blog to report the progress of the group, with information, e.g, about who and how many people in the LKCamp have patches merged in a given kernel release, how many patches, what problems those patches fixes or which feature it implements.  

If you want us to track your patches to the linux kernel, fill in the above spreadsheet the email you are going to use to send the patches to the community.  


