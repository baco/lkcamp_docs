# Phase 1: Structure

In phase 1, the meetings will be organized in checkpoints of knowledge. For example, in meeting M3 we assume most of the students already know what was studied in meetings M2 and M1. In this way we can have mini-presentation about the current subject and make sure the group reaches phase 2 all together.  

The main idea is to use this guide and make questions instead of having a teacher talking all the time. This way you will be able to follow your own pace and focus on the things you don't understand much.

**NOTE 1:** Newcomes will be always welcome in any point of the meetings. The material will be available for people to follow their on pace. The schedule exist mostly for guidance on the progression of the group and the current level that the mini-presentation can be given.  

**NOTE 2:** After achieving the expected checkpoint in a meeting, please try to help other students to reach the same checkpoint before going to the next one, so we keep the same level.  

**NOTE 3:** This document may not provide all the information you require or have a too short description with pointers. Making question is incentivated. Check session about "Where/How to make questions".  

